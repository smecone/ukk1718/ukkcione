<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
   
    public function __construct()
    {
        parent::__construct();

        $this->load->model('data_model');
    }

	public function index()
	{
        $query1['data'] = $this->data_model->getData();
        $this->load->view('template/header');
		$this->load->view('pages/index',$query1);
    }

    public function add()
    {
        $this->load->view('template/header');
		$this->load->view('pages/insert');
    }

    public function simpan()
    {
        $uname = $this->input->post('uname');
        $pw = $this->input->post('pw');
        $data = array(
            'user' => $uname,
            'pw' => $pw
        );
        $this->data_model->simpan($data);
        redirect('home');     

    }

    public function delete($id)
    {
        $this->data_model->delete($id);
        redirect('home');
    }

    public function update()
    {
       
        $this->load->view('template/header');
		$this->load->view('pages/insert');
    }

}
