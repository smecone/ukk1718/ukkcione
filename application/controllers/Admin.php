<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
   

    function __construct()
    {
        parent::__construct();  
        $this->load->model('datauser_model');
        $this->load->helper('url');
    }

        function index()
        {
            $this->load->view('template/login');
        }
      
       function view_user()
    {
        $data['userya'] = $this->datauser_model->tampil_user()->result();
        $this->load->view('template/master');
        $this->load->view('admin/index',$data);
    }

    function view_buku()
    {
        $this->load->view('template/master');
        $this->load->view('template/indexbuku');
    }

    function view_pinjam()
    {
        $this->load->view('template/master');
        $this->load->view('template/pinjam.php');
    }
    
    function view_adduser()
    {
        $this->load->view('template/master');
        $this->load->view('admin/adduser');
    }

    function view_addbuku()
    {
        $this->load->view('template/master');
        $this->load->view('admin/addbuku');
    }

    function view_statuspinjam()
    {
        $this->load->view('template/master');
        $this->load->view('template/statuspinjam');
    }

    function dashboard()
    {
        $this->load->view('template/master');
        $this->load->view('template/dashboard'); 
    }

        function test()
    {
            $this->load->view('template/master');
            $this->load->view('template/table');       
    }

    function adduser()
    {
        $this->load->view('admin/adduser');
    }

    function processadd(){
        $nama = $this->input->post('nama');
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $level = $this->input->post('level');  
      
        $data = array(
         'nama' => $nama,
         'username' => $username,
         'password' => $password,
         'level' => $level
         );
        $this->datauser_model->input_user($data,'tbl_user');
        redirect('admin/index');
       }

    function deleteuser($id)
    {
        $where = array('id' => $id);
        $this->datauser_model->delete_user($where,'tbl_user');
        redirect('admin/view_user');
    }

    function edituser($id)
    {
        $where = array('id' => $id);
        $data['userya'] = $this->datauser_model->edit_user($where,'tbl_user')->result();
        $this->load->view('admin/edituser',$data);
    }

    function update_user()
    {
        $id = $this->input->post('id');
        $nama = $this->input->post('nama');
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $level = $this->input->post('level');

        $data = array(
            'nama' => $nama,
            'username' => $username,
            'password' => $password,
            'level' => $level
        );

        $where = array(
            'id' => $id
        );

        $this->datauser_model->update_user($where,$data,'tbl_user');
        redirect('admin/index');
    }
}


