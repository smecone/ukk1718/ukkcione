<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
   


    function __construct()
    {
        parent::__construct();  
        $this->load->model('datausser');
        $this->load->helper('url');
    }
      
       function index()
    {
        $data['users'] = $this->datauser->tampil_user()->result();
        $this->load->view('user/index',$data);
    }

}


