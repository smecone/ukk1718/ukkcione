<main role="main" class="col-sm-9 ml-sm-auto col-md-10 pt-3">
<h2> Status Peminjaman Buku </h2>
<ol class="breadcrumb">
  <li class="breadcrumb-item"><a href="#">Admin</a></li>
  <li class="breadcrumb-item active">Status Peminjaman Buku</li>
</ol>

<!-- ini search -->
<div class="form-group">
<div class="col-md-5">
  <form class="form-inline" action="#">
    <input class="form-control mr-sm-2" type="text" placeholder="Search">
    <button class="btn btn-success" type="submit">Search</button>
  </form>
</div>
</div>
<!-- ini search -end -->

<div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>#Id Transaksi Pinjam </th>
                  <th>Id Peminjam</th>
                  <th>Id Buku</th>
                  <th>Nama Buku</th>
                  <th>Tanggal Pinjam</th>
                  <th>Tanggal Kembali</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>PJM001</td>
                  <td>0001</td>
                  <td>b001</td>
                  <td>Rumtuhnya Turki Utsmani</td>
                  <td>2013</td>
                  <td>2020</td>
                  <td>digantung</td>
                </tr>
               
                </tr>
              </tbody>
            </table>
          </div>
        </main>