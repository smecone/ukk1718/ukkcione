<html>
<head>
  <meta charset = "UTF-8">
  <link rel="stylesheet" href="<?php echo base_url('assets/css/stylelogin.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/css/w3.css');?>">
  <title>Login Page </title>
</head>
<body style="background-color: #e9e9e9; margin-bottom:10px;">
<h2 class="h2judul" a href="index.php"> Login Page LLibrary </h2>

<hr class="my-3">

<h6 class="alert" align="center"><?php echo @$_GET["invalid"] ?></h6>
<h6 class="alertijo" align="center"><?php echo @$_GET["logout"] ?></h6>
<h6 class="alertijo" align="center"><?php echo @$_GET["success"] ?></h6>


  <form action="<?php echo site_url('/admin/dashboard'); ?>" method="post" id="myForm">
  
    <div class="<module form-module">
        <br>
        </br>
          <label><b> Login Here </b></label>
        <br>
        <br>
        <input type="text" name="username" placeholder="Username" class="uname" required><br>
        <br>
        <input type="password" name="password" placeholder="Password" class="pwd" required><br>
        <br>
        <input class="btn btn-primary btn-md" type="submit" value="Login">
        
        <!-- Trigger/Open the Modal -->
        <button onclick="document.getElementById('id01').style.display='block'" class="btn btn-success">Register</button>
        
        <div class="lupas">
          <span>  © 2017 SPRL </span>
            
        </div>     

    </div>     
  </form>

  <!--Modal-->
  <div id="id01" class="w3-modal">
  <div class="w3-modal-content">
    <div class="w3-container">
      <span onclick="document.getElementById('id01').style.display='none'" 
         class="w3-closebtn">&times;</span>
         <div class="container">
         <h2 style="width:600px; margin-top:40px; margin-bottom:20px; margin-left:220px;"> LLibrary User Register's </h2>
         <form action="process_add.php" method="POST">
 
             <label for="name" style="margin-right:350px;">Nama</label>
             <input type="text" class="form-control" name="name" style="width:400px; margin-left:220px;">

             <label for="usernames" style="margin-right:320px;">Username</label>
             <input type="text" class="form-control" name="username" style="width:400px; margin-left:220px;">

             <label for="password" style="margin-right:320px;">Password</label>
             <input type="password" class="form-control" name="password" style="width:400px; margin-left:220px;">

             <label for="email" style="margin-right:350px;">Email</label>
             <input type="email" class="form-control" name="email" style="width:400px; margin-left:220px;" required="">

 
            

 
            <button type="submit" class="btn btn-success" name="addnewuser" style="width:300px; 
            height: 40px;
            width:195px;
            margin-top:20px;
            margin-bottom:50px;
            text-align:center;
            font-family: times new roman; 
            "> Daftar </button>
         </form>
 </div>
    </div>
</div>

  

</body>
</html>