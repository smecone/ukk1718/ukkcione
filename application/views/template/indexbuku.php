
<main role="main" class="col-sm-9 ml-sm-auto col-md-10 pt-3">


<title> Daftar Buku </title>
<ol class="breadcrumb">
  <li class="breadcrumb-item"><a href="#">Admin</a></li>
  <li class="breadcrumb-item active">Daftar Buku</li>
</ol>

<!-- ini search -->
<div class="form-group">
<div class="col-md-5">
  <form class="form-inline" action="#">
    <input class="form-control mr-sm-2" type="text" placeholder="Search">
    <button class="btn btn-success" type="submit">Search</button>
  </form>
</div>
</div>
<!-- ini search -end -->

    <table class="table table-hover">
    <thead>
      <tr>
        <th>ID Buku</th>
        <th>Nama Buku</th>
        <th>Pengarang</th>
        <th>Penerbit</th>
        <th>Tahun Penerbitan</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>John</td>
        <td>Doe</td>
        <td>john@example.com</td>
        <td>john@example.com</td>
        <td>john@example.com</td>
      </tr>
      <tr>
        <td>Mary</td>
        <td>Moe</td>
        <td>mary@example.com</td>
        <td>john@example.com</td>
        <td>john@example.com</td>
      </tr>
      <tr>
        <td>July</td>
        <td>Dooley</td>
        <td>july@example.com</td>
        <td>john@example.com</td>
        <td>john@example.com</td>
      </tr>
    </tbody>
  </table>

</main>