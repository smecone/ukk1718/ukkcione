<main role="main" class="col-sm-9 ml-sm-auto col-md-10 pt-3">

<title> PINJAM BUKU </title>

<ol class="breadcrumb">
  <li class="breadcrumb-item"><a href="#">Admin</a></li>
  <li class="breadcrumb-item active">Pinjam Buku</li>
</ol>

<h2> Pinjam Buku </h2>
<form class="form-horizontal" action="<?php echo base_url('index.php/admin/processadd'); ?>" method="post">

<fieldset>

<div class="form-group">
  <label class="col-md-4 control-label" for="txtnama">ID Transaksi Pinjam</label>  
  <div class="col-md-5">
  <input id="idpinjam" name="idpinjam" type="text" disabled value="000001" class="form-control input-md" required="">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="txtusername"> ID Peminjam </label>  
  <div class="col-md-5">
  <input id="txtusername" name="username" type="text"  class="form-control input-md" required="">

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="txtusername"> ID Buku </label>  
  <div class="col-md-5">
  <input id="txtusername" name="username" type="text"  class="form-control input-md" required="">
  </div>
</div>


<div class="form-group">
  <label class="col-md-4 control-label" for="txtusername"> Tanggal Pinjam </label>  
  <div class="col-md-5">
  <input id="txtusername" name="username" type="text"  value="12/12/2017" class="form-control input-md" required="">
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="txtusername"> Tanggal Kembali </label>  
  <div class="col-md-5">
  <input id="txtusername" name="username" type="text" value="12/12/2017" class="form-control input-md" required="">
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="btnsimpan"></label>
  <div class="col-md-4">
    <button id="btnsimpan" name="btnsimpan" class="btn btn-primary">Pinjam</button>
  </div>
</div>

</div>

<script> 
function myFunction() {
    var x = document.getElementById("passwordinput");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}
</script>
