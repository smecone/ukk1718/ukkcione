<div class="container-fluid">
      <div class="row">
        <nav class="col-sm-3 col-md-2 d-none d-sm-block bg-light sidebar">
          <ul class="nav nav-pills flex-column">
            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url('index.php/admin/dashboard  ');?>" <?php if($thisPage == "test") echo "class='active'"; ?> >Dashboard <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('index.php/admin/view_user  ');?>" <?php if($thisPage == "test") echo "class='active'"; ?> > Daftar User <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('index.php/admin/view_buku  ');?>" <?php if($thisPage == "test") echo "class='active'"; ?> > Daftar Buku <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('index.php/admin/view_adduser ');?>" <?php if($thisPage == "test") echo "class='active'"; ?> > Tambah Anggota <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('index.php/admin/view_addbuku ');?>" <?php if($thisPage == "test") echo "class='active'"; ?> > Tambah Buku <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('index.php/admin/view_pinjam');?>" <?php if($thisPage == "test") echo "class='active'"; ?> > Pinjam <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('index.php/admin/view_statuspinjam');?>" <?php if($thisPage == "test") echo "class='active'"; ?> > Status Peminjaman <span class="sr-only">(current)</span></a>
            </li>
          </ul>

        </nav>
      </div>
    </div>