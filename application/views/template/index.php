<html>
<head>
 <title>CRUD Menggunakan CodeIgniter || KiosCoding</title>
</head>
<body>

<main role="main" class="col-sm-9 ml-sm-auto col-md-10 pt-3">

<title> Daftar User </title>

<ol class="breadcrumb">
  <li class="breadcrumb-item"><a href="#">Admin</a></li>
  <li class="breadcrumb-item active">Daftar User</li>
</ol>
  
 <table style="margin:20px auto;" border="2" width="50%">
  <tr>
   <th>No</th>
   <th>Nama</th>
   <th>Id</th>
   <th>Username</th>
   <th>Level</th>
   <th>Password</th>
   <th>Action</th>
   
  </tr>
  <?php 
  $no = 1;
  foreach($userya as $s){ 
  ?>
  <tr>
   <td><?php echo $no++ ?></td>
   <td><?php echo $s->nama ?></td>
   <td><?php echo $s->id ?></td>
   <td><?php echo $s->username ?></td>
   <td><?php echo $s->level ?></td>
   <td><?php echo $s->password ?></td>
   <td>
        <?php echo anchor('admin/edituser/'.$s->id,'Edit'); ?>
        <?php echo anchor('admin/deleteuser/'.$s->id,'Hapus'); ?>
   </td>
  </tr>
  <?php } ?>
 </table>
 </main>
</body>
</html>



