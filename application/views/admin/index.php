<html>
<head>
<title> Daftar User </title>
</head>
<body>

<main role="main" class="col-sm-9 ml-sm-auto col-md-10 pt-3">


<ol class="breadcrumb">
  <li class="breadcrumb-item"><a href="#">Admin</a></li>
  <li class="breadcrumb-item active">Daftar User</li>
</ol>

<!-- ini search -->
<div class="form-group">
<div class="col-md-5">
  <form class="form-inline" action="#">
    <input class="form-control mr-sm-2" type="text" placeholder="Search">
    <button class="btn btn-success" type="submit">Search</button>
  </form>
</div>
</div>
<!-- ini search -end -->

 <table class="table table-hover">
  <tr>
   <th>No</th>
   <th>Nama</th>
   <th>Id</th>
   <th>Username</th>
   <th>Level</th>
   <th>Password</th>
   <th>Action</th>
   
  </tr>
  <?php 
  $no = 1;
  foreach($userya as $s){ 
  ?>
  <tr>
   <td><?php echo $no++ ?></td>
   <td><?php echo $s->nama ?></td>
   <td><?php echo $s->id ?></td>
   <td><?php echo $s->username ?></td>
   <td><?php echo $s->level ?></td>
   <td><?php echo $s->password ?></td>
   <td>
         <!-- <a class="btn btn-primary" href="<?php echo anchor('admin/edituser/'.$s->id,'Edit');?>"</a> -->

         <a href="<?php echo base_url();?>index.php/admin/edituser/<?php echo $s->id;?>" class="btn btn-info btn-sm"> Edit </a>
         <a href="<?php echo base_url();?>index.php/admin/deleteuser/<?php echo $s->id;?>" class="btn btn-danger btn-sm"> Hapus </a>

       
   </td>
  </tr>
  <?php } ?>
 </table>
</body>
</html>

