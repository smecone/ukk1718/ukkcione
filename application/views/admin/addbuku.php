<main role="main" class="col-sm-9 ml-sm-auto col-md-10 pt-3">

<title> TAMBAH BUKU </title>

<ol class="breadcrumb">
  <li class="breadcrumb-item"><a href="#">Admin</a></li>
  <li class="breadcrumb-item active">Tambah Buku</li>
</ol>

<h2> Tambah Buku </h2>
<form class="form-horizontal" action="<?php echo base_url('index.php/admin/processadd'); ?>" method="post">

<fieldset>

<div class="form-group">
  <label class="col-md-4 control-label" for="#">ID Buku</label>  
  <div class="col-md-5">
  <input id="#" name="#" type="text" placeholder="" class="form-control input-md" required="">
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="#">Nama Buku</label>  
  <div class="col-md-5">
  <input id="#" name="#" type="text" placeholder="" class="form-control input-md" required="">
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="#">Pengarang Buku</label>  
  <div class="col-md-5">
  <input id="#" name="#" type="text" placeholder="" class="form-control input-md" required="">
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="#">Penerbitan</label>  
  <div class="col-md-5">
  <input id="#" name="#" type="text" placeholder="" class="form-control input-md" required="">
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="#">Tahun Penerbitan</label>  
  <div class="col-md-5">
  <input id="#" name="#" type="text" placeholder="" class="form-control input-md" required="">
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="btnsimpan"></label>
  <div class="col-md-4">
    <button id="btnsimpan" name="btnsimpan" class="btn btn-primary">Simpan</button>
  </div>
</div>

</fieldset>
</form>
</div>

<script> 
function myFunction() {
    var x = document.getElementById("passwordinput");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}
</script>
