<main role="main" class="col-sm-9 ml-sm-auto col-md-10 pt-3">

<title> TAMBAH USER </title>

<ol class="breadcrumb">
  <li class="breadcrumb-item"><a href="#">Admin</a></li>
  <li class="breadcrumb-item active">Tambah Anggota</li>
</ol>

<h2> Form Anggota </h2>
<form class="form-horizontal" action="<?php echo base_url('index.php/admin/processadd'); ?>" method="post">

<fieldset>

<div class="form-group">
  <label class="col-md-4 control-label" for="txtnama">Nama</label>  
  <div class="col-md-5">
  <input id="txtnama" name="nama" type="text" placeholder="Nama" class="form-control input-md" required="">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="txtusername">Username</label>  
  <div class="col-md-5">
  <input id="txtusername" name="username" type="text" placeholder="Username" class="form-control input-md" required="">
    
  </div>
</div>

<!-- Password input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="passwordinput">Password</label>
  <div class="col-md-5">
    <input id="passwordinput" name="password" type="password" placeholder="*********" class="form-control input-md" required="">
    
    <input type="checkbox" onclick="myFunction()">Show Password
    
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="formlevel">Level</label>
  <div class="col-md-4">
    <select id="formlevel" name="level" class="form-control">
      <option value="admin">admin</option>
      <option value="user">user</option>
    </select>
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="btnsimpan"></label>
  <div class="col-md-4">
    <button id="btnsimpan" name="btnsimpan" class="btn btn-primary">Simpan</button>
  </div>
</div>

</fieldset>
</form>
</div>

<script> 
function myFunction() {
    var x = document.getElementById("passwordinput");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}
</script>
